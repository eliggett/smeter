# smeter

This project contains code to read, using voice, the s-meter from a typical GE repeater. The repeater must have the s-meter line (and ground) connected to an MPC3008 ADC. Over SPI, a raspberry pi can read and determine S and dB. 

## Hardware Circuit:

You will need an MPC3008 ADC. The required connects the GE s-meter signal, which is only a few tens of mV, to a transistor for amplification. I used a 2N2222 in grounded-emitter configuration, with the S-meter signal connected via a 33k resistor to the base terminal. The collector was tied to 3.3 volts using a 100k resistor. The sampled voltage is at the junction of the 100k resistor and the collector. You can change the readout range by changing this resistor. Ideally, a signal right by the repeater brings that 3.3 volts down to nearly zero, and a signal quite far away causes only a little drop in the Vc. (The program on the computer will un-invert the voltage, don't worry.)

This circuit has a lot of temperature dependency and is not what I would call "accurate". But it seems good enough and produces useful data. The temperature dependency comes from the transistor's shifting beta, causing a change in the VBE to Ic relationship. 

The reason for the circuit is because the ADC needs a signal that is a volt or so, and, we do not have the luxury of a negative voltage rail, needed for a more traditional amplifier circuit. Adding a switch-mode power supply to something so intimately connected to the radio's RF pathway seemed like a bad idea, and I didn't want to add another power supply or a battery. 

## Software code:

The code reads the ADC 1024 times, with 1ms between readings, for a total of about a second worth of data. An average is taken, and then the data is processed for a dB and then s-meter scale. You can easily modify the code to change the s-meter thresholds in the sreading(float dBsignal) function. The best strategy here is to go far away from your repeater, and then transmit a few levels of power from the same antenna. Write down the dB the repeater reports. You can then work out what the difference in dB should have been, and modify the code to match what you read out. Calibrated test equipment would of course be fine. The DN to dB conversion happens at this line:

dbfs = 20*log10((double)voltage/(double)vref);

near the end of smeter.c. 

## Asterisk configuration: 

This has only been tested in the so-called HamVoIP "distribution". 

Add the following line in your /etc/asterisk/rpt.conf file to your [functions 41751] stanza: 

84=cmd,/usr/local/sbin/saysignallevel.sh 41751

(my node number is 41751, yours will be different)

That will cause star 84 to read the signal level. The user must remain keyed for one second after sending the star 84 DTMF for the s-meter sampling and averaging. 
