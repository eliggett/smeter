/*
 * smeter.c is (c) 2018-2022 Elliott H. Liggett, W6EL, All rights reserved
 * smeter.c is licensed as GNU/GPL v3 and may be used per the terms of the
 * LICENSE file. 
 *
 * smeter.c is based on: 
 *  readMcp3008.c:
 *  read value from ADC MCP3008
 *
 * Requires: wiringPi (http://wiringpi.com)
 * Copyright (c) 2015 http://shaunsbennett.com/piblog
 ***********************************************************************
 */
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <stdbool.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

#define CHAN_CONFIG_SINGLE  8
#define CHAN_CONFIG_DIFF    0

#define AVGW 1024

static int myFd ;

void loadSpiDriver()
{
    printf("Loading SPI driver\n");
    if (system("gpio load spi") == -1)
    {
        fprintf (stderr, "Can't load the SPI driver: %s\n", strerror (errno)) ;
        exit (EXIT_FAILURE) ;
    }
}

void spiSetup (int spiChannel)
{
    if ((myFd = wiringPiSPISetup (spiChannel, 1000000)) < 0)
    {
        fprintf (stderr, "Can't open the SPI bus: %s\n", strerror (errno)) ;
        exit (EXIT_FAILURE) ;
    }
}

unsigned int sreading(float dBsignal)
{
    // Idle Noise (zero) is  = -59 to -60 dB. 
    // 6 dB per s-unit. 
    // Array contains criteria for S reading correspoding to index.
    // for example, if signal is greater than -6.5, S9. 
    const float sThresholds[9] = {  -6.5, // S9
                                    -12.5, // S8
                                    -18.5, // S7
                                    -24.5, // S6
                                    -30.5, // S5
                                    -36.5, // S4
                                    -42.5, // S3
                                    -48.5, // S2
                                    -54.5};// S1

    for(unsigned char s =0; s<9; s++)
    {
        if(  dBsignal > sThresholds[s])
        {
            return 9-s;
        }
    }

    return 0;
}

int myAnalogRead(int spiChannel,int channelConfig,int analogChannel)
{
    if(analogChannel<0 || analogChannel>7)
        return -1;
    unsigned char buffer[3] = {1}; // start bit
    buffer[1] = (channelConfig+analogChannel) << 4;
    wiringPiSPIDataRW(spiChannel, buffer, 3);
    return ( (buffer[1] & 3 ) << 8 ) + buffer[2]; // get last 10 bits
}

int main (int argc, char *argv [])
{
    //int loadSpi=FALSE;
    int analogChannel=0;
    int spiChannel=0;
    int channelConfig=CHAN_CONFIG_SINGLE;
    float voltage = 0;
    // float vreadings[AVGW];
    float vsum = 0;
    double dbfs;
    const float vref=3.30;
    int reading=0;

    wiringPiSetup () ;
    spiSetup(spiChannel);
    //
    analogChannel=0;
    for(int r=0; r < AVGW; r++)
    {
        // this function call takes approximately 45 microseconds
        // per query. 
        reading = myAnalogRead(spiChannel,channelConfig,analogChannel);

        voltage = (reading * vref) / 1024;
        voltage = 3.30 - voltage; // un-invert
        // vreadings[r] = voltage;
        vsum = vsum + voltage; // careful with large sums

        // delay should be total seconds of readings / number of readings
        // we should read as much as possible for the 
        // desired total time...
        // Then why delay at all? Sanity? 
        usleep(1000);
    }

    voltage = vsum / AVGW; 

    dbfs = 20*log10((double)voltage/(double)vref);
    // printf("Channel: %d, reading: %04d, voltage: %02.4f, dBfs: %02.4lf, S%d\n", analogChannel, reading, voltage, dbfs, sreading(dbfs) );
    printf("%02.2f,S%d\n", dbfs, sreading(dbfs));
    close (myFd) ;
    return 0;
}
