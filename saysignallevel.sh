#!/bin/bash
# This script is based on a script by Ramon Gonzalez,
# and is such a hack that it should be used carefully. 
# The script runs the smeter binary, parses out the dB and S-reading
# and then uses cat to combine the words "s meter" and "DB" with the numbers
# reported from the smeter binary. 
#
# Script to speak letters and numbers from asterisk sounds
# over a radio node using simpleusb
# by Ramon Gonzalez KP4TR 2014
#

#set -xv

ASTERISKSND=/var/lib/asterisk/sounds
LOCALSND=/tmp/randommsg
NODE=$1

function speak {
	sleep 2
        SPEAKTEXT=$(echo "$1" | tr '[:upper:]' '[:lower:]')
	echo "in speak 1: " $1
	echo "speaktext: " $SPEAKTEXT
        let SPEAKLEN=$(echo "$SPEAKTEXT" | /usr/bin/wc -m)-1
        COUNTER=0
        rm -f ${LOCALSND}.gsm
        touch ${LOCALSND}.gsm
	cat /var/lib/asterisk/sounds/letters/s.gsm >> ${LOCALSND}.gsm
	cat /var/lib/asterisk/sounds/meter.gsm >> ${LOCALSND}.gsm
        while [  $COUNTER -lt $SPEAKLEN ]; do
                let COUNTER=COUNTER+1
                CH=$(echo "$SPEAKTEXT"|cut -c${COUNTER})
		if [[ $CH =~ ^[A-Za-z_]+$ ]]; then
                        cat ${ASTERISKSND}/letters/${CH}.gsm >> ${LOCALSND}.gsm
                fi
                if [[ ${CH} =~ ^-?[0-9]+$ ]]; then
                        cat /var/lib/asterisk/sounds/digits/${CH}.gsm >> ${LOCALSND}.gsm
                fi

                case $CH in
                .) cat ${ASTERISKSND}/point.gsm >> ${LOCALSND}.gsm;;
                -) cat ${ASTERISKSND}/digits/minus.gsm >> ${LOCALSND}.gsm;;
                =) cat ${ASTERISKSND}/letters/equals.gsm >> ${LOCALSND}.gsm;;
                /) cat ${ASTERISKSND}/letters/slash.gsm >> ${LOCALSND}.gsm;;
                !) cat ${ASTERISKSND}/letters/exclaimation-point.gsm >> ${LOCALSND}.gsm;;
                @) cat ${ASTERISKSND}/letters/at.gsm >> ${LOCALSND}.gsm;;
                $) cat ${ASTERISKSND}/letters/dollar.gsm >> ${LOCALSND}.gsm;;
                *) ;;
                esac
        done

	echo "1: $1, soundfile: ${LOCALSND}"
	echo "-rx rpt localplay $NODE ${LOCALSND}"
	echo "NODE: " $NODE
        asterisk -rx "rpt localplay $NODE ${LOCALSND}"
}

if [ "$1" == "" ];then
	echo "Usage: programname.sh nodenumber"
        exit
fi

sig=`smeter`
strength=`echo $sig | cut -d, -f1`
sreading=`echo $sig | cut -d, -f2`


speak "$strength DB $sreading" $2
#speak "$1" $2

